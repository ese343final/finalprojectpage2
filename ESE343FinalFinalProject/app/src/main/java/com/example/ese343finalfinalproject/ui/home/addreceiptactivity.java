package com.example.ese343finalfinalproject.ui.home;

import com.example.ese343finalfinalproject.MainActivity;
import com.example.ese343finalfinalproject.R;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class addreceiptactivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button camera, save;
    private EditText text1, text2, text3, text4;
    private String name, category, date, stringprice;
    private Spinner spinner;
    private float price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addreceiptactivity);

        camera = (Button) findViewById(R.id.addphoto);
        save = (Button) findViewById(R.id.savereceipt);
        text1 =  findViewById(R.id.inputstore);
        //text2 =  findViewById(R.id.inputcategory);
        text3 = findViewById(R.id.inputdate);
        text4 = findViewById(R.id.inputprice);
        spinner = findViewById(R.id.categorydropdown);

        ArrayAdapter<CharSequence> adapter =
                ArrayAdapter.createFromResource(this, R.array.Categoryarray, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            name= text1.getText().toString();
            //category= text2.getText().toString();
            date = text3.getText().toString();
            stringprice = text4.getText().toString();
            price= Float.parseFloat(stringprice);

            Intent intent2 = new Intent(addreceiptactivity.this, MainActivity.class);
            intent2.putExtra("NameValue", name);
            intent2.putExtra("CategoryValue", category);
            intent2.putExtra("DateValue", date);
            intent2.putExtra("PriceValue", price);

            setResult(RESULT_OK, intent2);
            finish();


            }
        });




    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        category = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
