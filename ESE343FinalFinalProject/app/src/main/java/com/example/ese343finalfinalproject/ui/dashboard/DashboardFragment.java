package com.example.ese343finalfinalproject.ui.dashboard;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ese343finalfinalproject.MainActivity;
import com.example.ese343finalfinalproject.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private static String TAG = "MainActivity";

    private TextView Groceries_value, Food_value, Essentials_value, Bills_value, Subscriptions_value, Other_value;

    PieChart pie;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        //TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });


                Log.d(TAG, "onCreate: starting to create chart");

                pie = (PieChart) root.findViewById(R.id.piechart);

                pie.setDescription(" ");
                pie.setRotationEnabled(true);
                pie.setHoleRadius(25f);
                pie.setTransparentCircleAlpha(0);
                pie.setCenterText("Expenses");
                pie.setCenterTextSize(14);

                Groceries_value= root.findViewById(R.id.Groceryvalue);
                Food_value= root.findViewById(R.id.Foodvalue);
                Essentials_value= root.findViewById(R.id.Essentialsvalue);
                Bills_value= root.findViewById(R.id.Billsvalue);
                Subscriptions_value= root.findViewById(R.id.Subscriptionsvalue);
                Other_value= root.findViewById(R.id.Othervalue);

                Groceries_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[0]));
                Food_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[1]));
                Essentials_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[2]));
                Bills_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[3]));
                Subscriptions_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[4]));
                Other_value.setText("$" + String.valueOf(((MainActivity)getActivity()).totals[5]));

                addDataSet();

                pie.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                    @Override
                    public void onValueSelected(Entry e, Highlight h) {
                        Log.d(TAG, "onValueSelected: Value select from chart.");
                        Log.d(TAG, "onValueSelected: " + e.toString());
                        Log.d(TAG, "onValueSelected: " + h.toString());

                        int pos1 = e.toString().indexOf("(sum): ");
                        String sales = e.toString().substring(pos1 + 7);

                        for (int i = 0; i < ((MainActivity)getActivity()).totals.length; i++) {
                            if (((MainActivity)getActivity()).totals[i] == Float.parseFloat(sales)) {
                                pos1 = i;
                                break;
                            }
                        }
                        String employee = ((MainActivity)getActivity()).categories[pos1];

                    }

                    @Override
                    public void onNothingSelected() {

                    }



                });






        return root;
    }



    private void addDataSet(){
        Log.d(TAG, "addDataset started");

        Log.d(TAG, "addDataSet started");
        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for(int i = 0; i < ((MainActivity)getActivity()).totals.length; i++){
            yEntrys.add(new PieEntry(((MainActivity)getActivity()).totals[i] , i));
        }

        for(int i = 0; i < ((MainActivity)getActivity()).categories.length; i++){
            xEntrys.add(((MainActivity)getActivity()).categories[i]);
        }

        //create dataset
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "Categories");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);


        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.LTGRAY);
        colors.add(Color.GREEN);
        colors.add(Color.CYAN);
        colors.add(Color.YELLOW);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);

        //create pie data object
        PieData pieData = new PieData(pieDataSet);
        pieData.setValueFormatter(new twoDecimal(new DecimalFormat("######0.00")));
        pie.setData(pieData);
        pie.invalidate();


    }

    public class twoDecimal extends PercentFormatter {

        protected DecimalFormat mFormat = new DecimalFormat("0.00");;

        public twoDecimal(DecimalFormat format) {
            this.mFormat = format;
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return "$" + mFormat.format(value);
        }
    }


}
