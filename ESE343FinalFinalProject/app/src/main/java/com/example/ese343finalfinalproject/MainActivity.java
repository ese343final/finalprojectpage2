package com.example.ese343finalfinalproject;

import android.app.Application;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ese343finalfinalproject.ui.home.addreceiptactivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;




public class MainActivity extends AppCompatActivity {


    //Arrays that fragments use.
    // To access data in array in fragment you type for example
    // ((MainActivity)getActivity()).chartdata[0]
    //To modify data in array you type for example
    // ((MainActivity)getActivity()).chartdata[0] = (float) 40;



    public String[] categories= {"Groceries", "Food", "Essentials", "Bills", "Subscriptions", "Other"};
    public float[] totals = {0.00f, 0.00f, 0.00f, 0.00f, 0.00f, 0.00f};
    public ArrayList<String> receiptnames = new ArrayList<String>();
    public ArrayList<String> receiptcategory = new ArrayList<String>();
    public ArrayList<String> receiptdate = new ArrayList<String>();
    public ArrayList<Float> receiptprice = new ArrayList<Float>();







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);







    }




}
