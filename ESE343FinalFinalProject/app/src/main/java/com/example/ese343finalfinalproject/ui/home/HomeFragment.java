package com.example.ese343finalfinalproject.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ese343finalfinalproject.MainActivity;
import com.example.ese343finalfinalproject.R;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private Button add;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });


        //Actual code starts

        final ListView listresult = (ListView) root.findViewById(R.id.listResult);

        add = (Button) root.findViewById(R.id.addbutton);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonpressed();
            }
        });

        printlist(listresult);

        return root;
    }


    void buttonpressed(){

        Intent intent = new Intent(getActivity(), addreceiptactivity.class);
        startActivityForResult(intent, 1);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == -1){
                ListView listresult = (ListView) getView().findViewById(R.id.listResult);

                String name = data.getStringExtra("NameValue");
                String category = data.getStringExtra("CategoryValue");
                String date = data.getStringExtra("DateValue");
                float price = data.getFloatExtra("PriceValue", 0);
                ((MainActivity)getActivity()).receiptnames.add(name);
                ((MainActivity)getActivity()).receiptcategory.add(category);
                ((MainActivity)getActivity()).receiptdate.add(date);
                ((MainActivity)getActivity()).receiptprice.add(price);
                printlist(listresult);

                if(category.toLowerCase().equals("groceries")){
                    ((MainActivity)getActivity()).totals[0] += price;
                }
                else if(category.toLowerCase().equals("food")){
                    ((MainActivity)getActivity()).totals[1] += price;
                }
                else if(category.toLowerCase().equals("essentials")){
                    ((MainActivity)getActivity()).totals[2] += price;
                }
                else if(category.toLowerCase().equals("bills")){
                    ((MainActivity)getActivity()).totals[3] += price;
                }
                else if(category.toLowerCase().equals("subscriptions")){
                    ((MainActivity)getActivity()).totals[4] += price;
                }
                else if(category.toLowerCase().equals("other")){
                    ((MainActivity)getActivity()).totals[5] += price;
                }
                else{

                }


            }

        }
    }


    void printlist(ListView listresult){


        List<String> stringresult = new ArrayList<String>();

        if(((MainActivity)getActivity()).receiptnames.isEmpty()) {

            for (int i = 0; i < 1; i++) {


                String member = "";


                member +=
                        "No recent receipts added";
                stringresult.add(member);
            }
        }

        else{
            for (int i = ( ((MainActivity)getActivity()).receiptnames.size() -1); i >= 0; i--) {


                String member = "";


                member +=
                        "Store: " + ((MainActivity) getActivity()).receiptnames.get(i) + "\n";
                member +=
                        "Category: " + ((MainActivity) getActivity()).receiptcategory.get(i) + "\n";
                member +=
                        "Date: " + ((MainActivity) getActivity()).receiptdate.get(i) + "\n";
                member +=
                        "Price: " + ((MainActivity) getActivity()).receiptprice.get(i) + "\n";
                stringresult.add(member);
            }


        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1, android.R.id.text1, stringresult);

        listresult.setAdapter(adapter);

    }

}
